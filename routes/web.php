<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::get('/motorbrandf','FrontController@getBrand');
Route::get('/motortypef','FrontController@getType');
Route::get('/motorgallery','FrontController@motorGallery');
Route::post('/motorbook','FrontController@motorBook');
Route::get('/newsdetailfront/{id}','FrontController@NewsShowById');
// Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
//     ->name('ckfinder_connector');

// Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
//     ->name('ckfinder_browser');

Auth::routes([
    'register'=>false,
    'reset'=>false,
    'verify'=>false
    ]);

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
    // list all lfm routes here...
});
//booking controller
Route::get('/createbookingadmin', 'BookingController@createBookingAdmin');


//motor controller
Route::get('/motor','MotorController@index')->name('motor');
Route::get('/motorshowall','MotorController@ShowAll');
Route::get('/motordetail/{id}','MotorController@ShowById');
Route::get('/motorinput','MotorController@createMotor');
Route::post('/motorsave','MotorController@storeMotor');
Route::get('/motoredit/{id}','MotorController@editMotor');
Route::post('/motorupdate','MotorController@updateMotor');
Route::get('/motordelete/{id}','MotorController@destroyMotor');
//motor type sub controler
Route::get('/motortypesetting','MotorController@indexType')->name('motortypesetting');
Route::get('/motortypeinput','MotorController@createType');
Route::post('/motortypesave','MotorController@storeType');
Route::get('/motortype','MotorController@getType');
Route::get('/motortypedelete/{id}','MotorController@destroyType');
//motor brand controller
Route::get('/motorbrandsetting','MotorController@indexBrand')->name('motorbrandsetting');
Route::get('/motorbrandinput','MotorController@createBrand');
Route::post('/motorbrandsave','MotorController@storeBrand');
Route::get('/motorbrand','MotorController@getBrand');
Route::get('/motorbranddelete/{id}','MotorController@destroyBrand');
//booking controller

//customer controller

//product controller

//news controller
Route::get('/news','NewsController@index')->name('news');
Route::get('/newsinput','NewsController@create');
Route::post('/newssave','NewsController@store');
Route::get('/newsshowall','NewsController@ShowAll');
Route::get('/newsdetail/{id}','NewsController@ShowById');
Route::get('/newsedit/{id}','NewsController@edit');
Route::post('/newsupdate','NewsController@update');
Route::get('/newsdelete/{id}','NewsController@destroy');
//setting controller
Route::get('/editpass','SettingController@editPass');
