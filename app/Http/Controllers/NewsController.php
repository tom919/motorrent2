<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('news/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('/news/input');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

         //validation
         $request->validate([
            'title' => 'required',
            'pic' => 'required|image|mimes:jpeg,png,jpg',
            'status' => 'required'
        ],[
            'title.required'=>'Title Required',
            'pic.required'=>'News Photo Caption Required',
            'status.required' => 'News status required'
        ]);

        //get data
        $title=$request->input('title');
        $content=$request->input('content');
        $status=$request->input('status');   
        $createdDate=date("Y-m-d");
        $rand=rand(1000000,9999999);
        // pic processing
        $pic = $rand.'.'.$request->pic->getClientOriginalExtension();
        $request->pic->move(public_path('newscap'), $pic);
        //query
        $results = DB::select( DB::raw("CALL InsertNews( '$title', '$createdDate', '$content', '$pic','$status')") );



        //response
        return redirect()->route('news')->with('responsemessage',$results[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ShowById($id)
    {
        $results = DB::select("CALL NewsDetail(?)",[$id]);
        return response()->json([$results[0]]);
        //
    }
     /* display all data */
     public function ShowAll(Request $request)
     {
 
         $limit=10;
         $start=$request->input('start')*$limit;
         if($request->input('search.value'))
             {
                 $search=$request->input('search.value');
             }
         else
             {
                 $search='';
             }
         $results = DB::select("CALL ShowNewsAll(?,?,?)",[$start,$limit,$search] );
         $counter_listing=DB::table('article')->count();
         $counter_filter=ceil($counter_listing/$limit);
         return response()->json(["data"=>$results,"draw"=>$request->input('draw'),"recordsTotal"=>$counter_listing,"recordsFiltered"=>$counter_filter]);
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $results = DB::select("CALL NewsDetail(?)",[$id]);
        return view('/news/edit',["data"=>$results[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
         //validation
         $request->validate([
            'title' => 'required',
            'status' => 'required'
        ],[
            'title.required'=>'Title Required',
            'status.required' => 'status required'
        ]);

        //get data
        $id=$request->input('id');
        $title=$request->input('title');
        $content=$request->input('content');
        $status=$request->input('status');   
        $updateDate=date('Y-m-d H:i:s', time());

        if($request->hasFile('photocap')){
            $file = public_path('newscap/').$request->input('photocapid');
            if(file_exists($file)==1)
            {
                unlink($file);

            }
            
          $photocap = $title.'.'.$request->photocap->getClientOriginalExtension();
          $request->photocap->move(public_path('newscap'), $photocap);
       } else{
           $photocap = $request->input('photocapid');
       }



        $results = DB::select("CALL UpdateNews(?,?,?,?,?,?)",[$id, $title , $updateDate, $content, $photocap, $status]);
        return redirect()->route('news')->with('responsemessage',$results[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mtr= DB::select("SELECT * FROM article WHERE id='$id'");
        $file = public_path('newscap/').$mtr[0]->photocap;
        if(file_exists($file)==1)
        {
            unlink($file);

        }

        $results = DB::select("CALL DeleteNews(?)",[$id]);
        return redirect()->route('news')->with('responsemessage',$results[0]);
    }
}
