<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\News as News;

class FrontController extends Controller
{
    //
    public function index()
    {

        // $data= DB::select("SELECT * FROM article ORDER BY created_date");
        $data = News::where('status', 'Active')
               ->orderBy('created_date', 'desc')
               ->take(3)
               ->get();
               
        return view('mainhome')->with('data',$data);
    }

    /* display all data */
    public function NewsShowById($id)
    {

        $results = DB::select("CALL NewsDetail(?)",[$id]);
        return response()->json([$results[0]]);
    }

    public function motorGallery()
    {
        return view('motor');
    }

    public function getType()
    {
        $results = DB::select("SELECT id, name FROM motor_type");
        return response()->json($results);
    }

    public function getBrand()
    {
        $results = DB::select("SELECT id, brand_name from motor_brand");
        return response()->json($results);
    }

    public function motorBook(Request $request)
    {
            //   //validation
            //   $request->validate([
            //     'name' => 'required',
            //     'email' => 'required',
            //     'phone' => 'required',
            //     'startDate' => 'required',
            //     'endDate' => 'required',
            //     'motorBrand' => 'required',
            //     'motorType' => 'required'
            // ],[
            //     'name.required'=>'Name Required',
            //     'email.required'=>'Email Required',
            //     'phone.required'=>'Phone Required',
            //     'startDate.required'=>'Start Book Date Required',
            //     'endDate.required'=>'End Book Date Required',
            //     'motorBrand.required' => 'Motor brand required',
            //     'motorType.required' => 'Motor type required'
            // ]);

        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');
        $motorBrand = $request->input('brand');
        $motorType = $request->input('type');

        $sender = "booking@rapahdirtbike.com";
        $subject = "Booking Detail";
        $officesubject ="New Booking";
        $officemail = "professional.it919@gmail.com";
        $data = array('name'=>$name, 'email'=> $email, 'phone'=>$phone, 'startDate'=>$startDate, 'endDate'=>$endDate, 'motorBrand'=>$motorBrand, 'motorType'=>$motorType);

                Mail::send('mail', $data, function($message) use ($email, $subject) {
                    $message->to($email)->subject($subject);
                    $message->from('booking@rapahdirtbike.com','RapahDirtBikeRental Booking');
                });

                
                Mail::send('mailnotif', $data, function($message) use ($officemail, $officesubject) {
                    $message->to($officemail)->subject($officesubject);
                    $message->from('booking@rapahdirtbike.com','RapahDirtBikeRental Booking');
                });

        // $res = ['name'=>$name,'email'=>$email, 'phone'=>$phone, 'startDate'=>$startDate, 'endDate'=>$endDate, 'motorBrand'=>$motorBrand,'motorType'=>$motorType];
        return back()->with('alert-success','Thanks For booking, please check your email');
    }
}
