<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function editPass()
    {
        // $data= DB::select("SELECT password FROM users WHERE id=1");
        return view('/settings/editpass');
    }

    public function updatePass(Request $request)
    {
                //validation
                $request->validate([
                    'oldPass' => 'required',
                    'newPass' => 'required|same:reNewPass',
                    'reNewPass' => 'required'
                ],[
                    'oldPass.required'=>'Old Password Required',
                    'newPass.required'=>'New Password Required',
                    'reNewPass.required'=>'Retype New Password Required'
                ]);
        
                //get data
                $oldPass= Hash::make($request->input('oldPass'));
                //check old pass query
                $dataPass= DB::select("SELECT password FROM users WHERE id=1");
                if($oldPass!=$dataPass)
                    {
                        return redirect()->route('home')->with('responsemessage','Old Password Not Match');
                    }

                
                $newPass=$request->input('NewPass');
                $reNewpass=$request->input('reNewPass');
                
        
                $results = DB::select("Update users SET password=? WHERE id=1 ",[$newPass]);
                return redirect()->route('home')->with('responsemessage',$results[0]);
    }
}
