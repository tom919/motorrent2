@extends('layouts.newslayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">News Management</div>
                @if (session('responsemessage'))
                            @foreach (session('responsemessage') as $rsp)
                            <div class="alert alert-success">
                            {{$rsp}}
                            </div>
                            @endforeach
                    @endif

                <div class="card-body">

                <div class="row">
                    <div class="col-12" >
                        <div class="pull-right p-2">
                        <a href="{{url('/newsinput')}}"><button class="btn btn-info btn-outline btn-md"><i class="fa fa-pencil-square"></i> Create News</button></a>
                        </div>
                        <table id="example" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Created Date</th>
                                    <th>Update Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                        </table>
                    </div>

                </div>
                </div>
            </div>
        </div>
    </div>
</div>


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title pull-left">Detail</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="row pull-right">
            <div id="pedit">   
            </div>
            <div id="pdel" class="del">   
            </div>
        </div>
                  <div class="table-responsive" id = "div1" style = " width: 100%; margin-right: 10%;  background-color: white;">
                  <table class="table borderless">
                      <tbody id="myTable">
                          <tr style = "text-align: center;"  data-toggle="modal" data-id="" data-target="#orderModal">
                              <td>Id</td><td id="pid"></td>
                          </tr>
                          <tr style = "text-align: center;"  data-toggle="modal" data-id="" data-target="#orderModal">
                              <td>Title</td><td id="ptitle"></td>
                          </tr>
                          <tr style = "text-align: center;"  data-toggle="modal" data-id="" data-target="#orderModal">
                              <td>Created Date</td><td id="pcreated"></td>
                          </tr>
                          <tr style = "text-align: center;"  data-toggle="modal" data-id="" data-target="#orderModal">
                              <td>Update Date</td><td id="pupdate"></td>
                          </tr>
                          <tr style = "text-align: center;"  data-toggle="modal" data-id="" data-target="#orderModal">
                              <td>Content</td><td id="pcontent"></td>
                          </tr>
                          <tr style = "text-align: center;"  data-toggle="modal" data-id="" data-target="#orderModal">
                              <td></td><td id="ppic"></td>
                          </tr>
                          <tr style = "text-align: center;"  data-toggle="modal" data-id="" data-target="#orderModal">
                              <td>Status</td><td id="pstatus"></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
        </div>
    
      </div>

    </div>
  </div>
@endsection
