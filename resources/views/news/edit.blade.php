@extends('layouts.newslayout2')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit News</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <form action="{{url('/newsupdate')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <!--- update form -->

                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="photocapid" value="{{$data->photocap}}">
                    <div class="form-group m-md-5">
                            <div class="row">
                                <label>Title:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="title" class="form-control m-md-3"  value="{{$data->title}}"/>
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Content:</label>
                            </div>
                            <div class="row">
                        
                                <textarea name="content" id="content" cols="30" rows="10">{{$data->content}}</textarea> 
                              
                            </div>
                        </div>
                     
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Photo:</label>
                            </div>
                            <div class="row">
                            <img src='{{url("/newscap")}}/{{$data->photocap}}' style="max-width: 50%" >
                                <input type="file" name="photocap" class="form-control m-md-3" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Status:</label>
                            </div>
                            <div class="row">
                               <select class="form-control m-md-3" name="status">
                               
                               <option selected value="{{$data->status}}">{{$data->status}}</option>
                               <option value="Active">Active</option>
                               <option value="Suspend">Suspend</option>
                               <option value="Hidden">Hidden</option>
                               </select>
                            </div>
                        </div>
                        <div class="form-group m-md-5">

                            <div class="col-md-10"></div>
                            <div class="col-md-2 pull-right">
                                <button type="submit" class="btn btn-primary btn-outline">
                                <span class="glyphicon glyphicon-floppy-disk"></span>
                                Update</button>
                            </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
