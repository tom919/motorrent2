@extends('layouts.newslayout2')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Motor</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <form action="{{url('/newssave')}}" method="post" enctype="multipart/form-data">
                    @csrf

                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Title:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="title" class="form-control m-md-3" placeholder="news title" />
                            </div>
                        </div>
             
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Content:</label>
                            </div>
                            <div class="row">
                                <textarea name="content" id="content" class="form-control m-md-3" rows="10" cols="50"></textarea>
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Photo Caption:</label>
                            </div>
                            <div class="row">
                                <input type="file" name="pic" class="form-control m-md-3" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Status:</label>
                            </div>
                            <div class="row">
                               <select class="form-control m-md-3" name="status">
                               <option selected>Select status</option>
                               <option value="Active">Active</option>
                               <option value="Suspend">Suspend</option>
                               <option value="Hidden">Hidden</option>
                               </select>
                            </div>
                        </div>
                        <div class="form-group m-md-5">

                            <div class="col-md-10"></div>
                            <div class="col-md-2 pull-right">
                                <button type="submit" class="btn btn-primary btn-outline">
                                <span class="glyphicon glyphicon-floppy-disk"></span>
                                Save</button>
                            </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
