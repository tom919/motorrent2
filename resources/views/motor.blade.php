
@extends('layouts.frontlayout')

@section('content')

<!-- portfolio-inner-page -->
<div id="portfolio" class="portfolio">
		<div class="heading">
			<h3>Motor
			<div class="w3ls-button">
				<a href="#" onclick="InputBook()">Book Now</a>
			</div>
		</h3>
		</div>
		<div class="container">
			<div class="sap_tabs">			
				<div id="horizontalTab">
					<ul class="resp-tabs-list">
						<li class="resp-tab-item"><span>Scooter</span></li>
						<li class="resp-tab-item"><span>Big Scooter</span></li>
						<li class="resp-tab-item"><span>Offroad</span></li>
						<li class="resp-tab-item"><span>Sports</span></li>					
					</ul>	
					<div class="clearfix"> </div>	
					<div class="resp-tabs-container">
						<!--- scooter -->
						<div class="tab-1 resp-tab-content">
					
							<div class="col-md-4 col-sm-4 portfolio-grids portfolio-grid1">
								<a href="{{url('maintemp/motorimg/scooter1.jpg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/scooter1.jpg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h2>Scooter</h2>
										Book here
									</div>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 portfolio-grids portfolio-grid2">
								<a href="{{url('maintemp/motorimg/scooter2.jpg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/scooter2.jpg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Scooter</h5>
									</div>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 portfolio-grids portfolio-grid3">
								<a href="{{url('maintemp/motorimg/scooter3.jpg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/scooter3.jpg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Scooter</h5>
									</div>
								</a>
							</div>
							
							<div class="clearfix"> </div>
						</div>
						<!-- big scooter -->		
						<div class="tab-1 resp-tab-content">
							<div class="col-md-4 col-sm-4 portfolio-grids portfolio-grid1">
								<a href="{{url('maintemp/motorimg/bigscooter1.jpg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/bigscooter1.jpg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Big Scooter</h5>
									</div>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 portfolio-grids portfolio-grid2">
								<a href="{{url('maintemp/motorimg/bigscooter2.png')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/bigscooter2.png')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Big Scooter</h5>
									</div>
								</a>
							</div>
							<div class="clearfix"> </div>
						</div>
						<!--- offroad -->
						<div class="tab-1 resp-tab-content">
						
							<div class="col-md-4 col-sm-4 portfolio-grids portfolio-grid1">
								<a href="{{url('maintemp/motorimg/offroad1.jpeg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/offroad1.jpeg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Offroad Bike</h5>
									</div>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 portfolio-grids  portfolio-grid2">
								<a href="{{url('maintemp/motorimg/offroad2jpg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/offroad2.jpg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Offroad</h5>
									</div>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 portfolio-grids  portfolio-grid3">
								<a href="{{url('maintemp/motorimg/offroad3.jpg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/offroad3.jpg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Offroad</h5>
									</div>
								</a>
							</div>
						
							<div class="clearfix"> </div>
						</div>
						<!--- sport -->
						<div class="tab-1 resp-tab-content">
							<div class="col-md-4 col-sm-4 portfolio-grids portfolio-grid1">
								<a href="{{url('maintemp/motorimg/sport1.jpeg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/sport1.jpeg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Sport Bike</h5>
									</div>
								</a>
							</div>
							
							<div class="col-md-4 col-sm-4 portfolio-grids portfolio-grid2">
								<a href="{{url('maintemp/motorimg/sport2.jpeg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/sport2.jpeg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Sport Bike</h5>
									</div>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 portfolio-grids  portfolio-grid3">
								<a href="{{url('maintemp/motorimg/sport3.jpg')}}" data-lightbox="example-set" data-title="">
									<img src="{{url('maintemp/motorimg/sport3.jpg')}}" class="img-responsive zoom-img" alt=""/>
									<div class="b-wrapper">
										<h5>Sport Bike</h5>
									</div>
								</a>
							</div>
						
							<div class="clearfix"> </div>
						</div>
					</div>						
				</div>
			</div>
		</div>
	</div>
	<!-- //portfolio-inner-page -->
 <!-- footer -->
 <footer>
		<div class="agileits-w3layouts-footer">
			<div class="container">
				<div class="col-md-4 w3-agile-grid" id="aboutUs">
					<h5>About Us</h5>
					<p>Trusted and reliable motor rental</p>
					<div class="footer-agileinfo-social">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
							<li><a href="#"><i class="fa fa-vk"></i></a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-md-4 w3-agile-grid">
					<h5>Address</h5>
					<div class="w3-address">
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Phone Number</h6>
								<p>+6288888888</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Email Address</h6>
								<p>Email :<a href="mailto:example@email.com"> cs@rapahdirtbikerental.com</a></p>
							</div>
							<div class="clearfix"> </div>
						</div>
				
					</div>
				</div>
				<div class="col-md-4 w3-agile-grid">
					<h5>Locations</h5>
					<div class="w3ls-post-grids">
						<div class="w3ls-post-grid">
						<div class="w3-address-left">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Location</h6>
								<p> Jl Raya Menanga 
								Telephone : +62 8588888888
								</p>
							</div>
							<div class="clearfix"> </div>	
						
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="copyright">
			<div class="container">
				<p>© 2020 RapahDirtBikeRental. All rights reserved </p>
			</div>
		</div>
	</footer>
	<!-- //footer -->

  <!-- Modal -->
  <div class="modal fade" id="bookModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
     
		<div class="banner-form-agileinfo">
									<h5>Book your bike Now</h5>
									<p></p>
									<form action="#" method="post">
										<input type="text" class="email" name="name" placeholder="Name" required="">
                                        <input type="tel" class="tel" name="tel" placeholder="Phone Number" required="">
										<input type="text" class="email" name="email" placeholder="Email" required="">
										<input type="text" id="startDate" name="startDate" placeholder="From" required="">
										<input type="text" id="endDate" name="endDate" placeholder="To" required="">
										<select class="form-control m-md-3" name="brand" id="motorBrand" >
                               			</select>
										   <select class="form-control m-md-3" name="type" id="motorType" >
                               			</select>
										<input type="submit" class="hvr-shutter-in-vertical" value="Book">  	
									</form>
								</div>
        </div>
    
      </div>

    </div>
  </div>
  <script type="text/javascript" src="{{asset('js/jquery341.js')}}"></script>
	<script src="{{ asset('js/bootstrap341.js')}}"></script>
  <script src="{{asset('maintemp/js/lightbox-plus-jquery.min.js')}}"> </script>
  <script src="{{url('maintemp/js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{url('maintemp/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{url('maintemp/js/easing.js')}}"></script>
<script src="{{asset('maintemp/js/easyResponsiveTabs.js')}}" type="text/javascript"></script>
  <script>
	  	$('#horizontalTab').easyResponsiveTabs({
						type: 'default', //Types: default, vertical, accordion           
						width: 'auto', //auto or any width like 600px
						fit: true   // 100% fit in a container
					});	
	  </script>
	  <!-- start-smoth-scrolling -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
@endsection

