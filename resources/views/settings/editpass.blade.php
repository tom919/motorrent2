@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Password</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <form action="#" method="post" enctype="multipart/form-data">
                    @csrf

                    <!--- update form -->

   
                    <div class="form-group m-md-5">
                            <div class="row">
                                <label>Old Password:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="oldPass" class="form-control m-md-3" placeholder="Old Password" />
                            </div>
                        </div>

                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>New Password:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="newPass" class="form-control m-md-3" placeholder="New Password"/>
                            </div>
                        </div>

                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Re Type New Password:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="reNewPass" class="form-control m-md-3" placeholder="Re Type New Password"/>
                            </div>
                        </div>
                     
                      
                        <div class="form-group m-md-5">

                            <div class="col-md-10"></div>
                            <div class="col-md-2 pull-right">
                                <button type="submit" class="btn btn-primary btn-outline">
                                <span class="fa fa-check-circle-o"></span>
                                Update</button>
                            </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
