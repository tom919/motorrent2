@extends('layouts.motorlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Motor</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <form action="{{url('/motorupdate')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <!--- update form -->

                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="picid" value="{{$data->pic}}">
                    <div class="form-group m-md-5">
                            <div class="row">
                                <label>Reg Number:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="regNumber" class="form-control m-md-3" placeholder="XX XXXX XXX" value="{{$data->reg_number}}"/>
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Type:</label>
                            </div>
                            <div class="row">
                                <select class="form-control m-md-3" name="type" id="motorType" >
                                <option value="{{$data->mtid}}">{{$data->motor_type}}</option>
                               </select>
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Brand:</label>
                            </div>
                            <div class="row">
                               <select class="form-control m-md-3" name="brand" id="motorBrand" >
                               <option value="{{$data->mbid}}">{{$data->motor_brand}}</option>
                               </select>
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Year:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="year" id="motoryear" class="form-control m-md-3" placeholder="New Type" value="{{$data->year}}" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Rate (IDR):</label>
                            </div>
                            <div class="row">
                                <input type="text" name="rate" class="form-control m-md-3" placeholder="Rate" value="{{$data->rate}}" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Photo:</label>
                            </div>
                            <div class="row">
                            <img src='{{url("/motorpic")}}/{{$data->pic}}' style="max-width: 50%" >
                                <input type="file" name="pic" class="form-control m-md-3" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Status:</label>
                            </div>
                            <div class="row">
                               <select class="form-control m-md-3" name="status">
                               
                               <option selected value="{{$data->status}}">{{$data->status}}</option>
                               <option value="Available">Avaliable</option>
                               <option value="Maintenance">Maintenance</option>
                               <option value="Occupied">Occupied</option>
                               <option value="Broken">Broken</option>
                               </select>
                            </div>
                        </div>
                        <div class="form-group m-md-5">

                            <div class="col-md-10"></div>
                            <div class="col-md-2 pull-right">
                                <button type="submit" class="btn btn-primary btn-outline">
                                <span class="glyphicon glyphicon-floppy-disk"></span>
                                Update</button>
                            </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
