
@extends('layouts.frontlayout')

@section('content')
			@if(\Session::has('alert-success'))
                <div class="alert alert-success">
                    <div>{{Session::get('alert-success')}}</div>
                </div>
            @endif
<!-- Slider -->
<div class="slider">
	<div class="callbacks_container">
		<ul class="rslides" id="slider">
			<li>
				<div class="w3layouts-banner-top w3layouts-banner-top1">
					<div class="banner-dott">
					<div class="container">
						<div class="slider-info">
							<div class="col-md-8">
								<h2>Offroad Bike</h2>
								<h4> </h4>
								<div class="w3ls-button">
									<a href="#" onclick="InputBook()">Book Now</a>
								</div>
					
							</div>
							<div class="col-md-4">
							<!-- @include('booking') -->
							</div>
						</div>
					</div>
					</div>
				</div>
			</li>
			<li>
				<div class="w3layouts-banner-top">
					<div class="banner-dott">
					<div class="container">
						<div class="slider-info">
							<div class="col-md-8">
								<h3>Big Scooter</h3>
								<h4></h4>
								<div class="w3ls-button">
									<a href="#" onclick="InputBook()">Book Now</a>
								</div>
					
							</div>
							<div class="col-md-4">
							<!-- @include('booking') -->
							</div>
						</div>
					</div>
					</div>
				</div>
			</li>
			<li>
				<div class="w3layouts-banner-top w3layouts-banner-top3">
					<div class="banner-dott">
					<div class="container">
						<div class="slider-info">
							<div class="col-md-8">
								<h3>Sport Bike</h3>
								<h4></h4>
								<div class="w3ls-button">
									<a href="#" onclick="InputBook()">Book Now</a>
								</div>
						
							</div>
							<div class="col-md-4">
							<!-- @include('booking') -->
							</div>
						</div>
					</div>
					</div>
				</div>
			</li>
			<li>
				<div class="w3layouts-banner-top w3layouts-banner-top2">
					<div class="banner-dott">
					<div class="container">
						<div class="slider-info">
							<div class="col-md-8">
								<h3>Scooter</h3>
								<h4></h4>
								<div class="w3ls-button">
									<a href="#" onclick="InputBook()">Book Now</a>
								</div>
								
							</div>
							<div class="col-md-4">
							<!-- @include('booking') -->
							</div>
						</div>
					</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div class="clearfix"></div>
</div>
<!-- //Slider -->				
<!-- bootstrap-modal-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div id="ptitle"></div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
					<div class="modal-body">
						<div class="pcap"></div>
						<p id="pcreated"></p>
						<p>
							<div id="pcontent"></div>
						</p>
					</div>
			</div>
		</div>
	</div>
<!-- //bootstrap-modal-pop-up --> 
<!-- banner-bottom -->
<div class="banner-bottom">
	<div class="col-md-7 bannerbottomleft">
			<div class="video-grid-single-page-agileits">
				<div data-video="7gKfVjmKpts" id="video"> <img src="{{url('maintemp/images/besakih.jpg')}}" alt="" class="img-responsive" /> </div>
			</div>
	</div>
	<div class="col-md-5 bannerbottomright">
		<h3>Why choose us?</h3>
		<p></p>
		<h4><i class="fa fa-star" aria-hidden="true"></i>Clean Bike</h4>
		<h4><i class="fa fa-dollar" aria-hidden="true"></i>Affordable price</h4>
		<h4><i class="fa fa-gears" aria-hidden="true"></i>Guaranted engine maintenance</h4>
		<h4><i class="fa fa-map-marker" aria-hidden="true"></i>Local Guide</h4>
	</div>
	<div class="clearfix"></div>
</div>
<!-- //banner-bottom -->

<!-- Clients -->
	<div class=" col-md-6 clients">
			<h3>Recomended Bike</h3>
			<section class="slider">
				<div class="flexslider">
					<ul class="slides">
						<li>
								<div class="client">					
									<h5>Kawasaki KLX 150</h5>	
									<div class="clearfix"> </div>
								</div>
								<br>
								<img src="{{url('maintemp/images/beat.jpg')}}" alt="" />
		
								
						</li>
						<li>	
								<div class="client">
									<h5>Suzuki Z450</h5>
									<div class="clearfix"> </div>
								</div>
								<br>
								<img src="{{url('maintemp/images/bigscooter.jpg')}}" alt="" />
								
						</li>
						<li>
								<div class="client">
									<h5>Yamaha WR250R</h5>
									<div class="clearfix"> </div>
								</div>
								<br>
								<img src="{{url('maintemp/images/trail.jpg')}}" alt="" />
						
						</li>
						<li>
								<div class="client">

									<h5>KTM 250</h5>
									<div class="clearfix"> </div>
								</div>
								<br>
								<img src="{{url('maintemp/images/sport.jpg')}}" alt="" />
							
						</li>
					</ul>
				</div>
			</section>
</div>
<!-- //Clients -->
<!-- Counter -->
	<div class="col-md-6 services-bottom">
			<div class="col-md-6 agileits_w3layouts_about_counter_left">
				<div class="countericon">
					<i class="fa fa-motorcycle" aria-hidden="true"></i>
				</div>
				<div class="counterinfo">
					<p class="counter">6</p> 
					<h3>Offroad Bike</h3>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-6 agileits_w3layouts_about_counter_left">
				<div class="countericon">
					<i class="fa fa-motorcycle" aria-hidden="true"></i>
				</div>
				<div class="counterinfo">
					<p class="counter">4</p> 
					<h3>Big Scooter</h3>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
			<div class="col-md-6 agileits_w3layouts_about_counter_left">
				<div class="countericon">
					<i class="fa fa-motorcycle" aria-hidden="true"></i>
				</div>
				<div class="counterinfo">
					<p class="counter">5</p>
					<h3>Scooter</h3>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-6 agileits_w3layouts_about_counter_left">
				<div class="countericon">
					<i class="fa fa-motorcycle" aria-hidden="true"></i>
				</div>
				<div class="counterinfo">
					<p class="counter">2</p>
					<h3>Sport Bike</h3>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
	</div>
			<div class="clearfix"> </div>
<!-- //Counter -->


<!-- our blog -->
<section class="blog" id="blog">
	<div class="container">
		<div class="heading">
			<h3>Latest News</h3>
		</div>
		<div class="blog-grids">
		@foreach($data as $news)
		<!-- start of news loop -->
		<div class="col-md-4 blog-grid">
			<a href="#" onclick='GetNewsDetail("{!! json_encode($news->id) !!}")'><img src="{{url('/')}}/newscap/{{$news->photocap}}" alt="" /></a>
			<p>{{$news->created_date}}</p>
			<h3><a href="#" data-toggle="modal" data-target="#myModal">{{$news->title}}</a></h3>
			<p> {!! Str::limit($news->content, 50, '...')!!}</p>
			<div class="readmore-w3">
				<a class="readmore" href="#"  onclick='GetNewsDetail("{!! json_encode($news->id) !!}")'>Read More</a>
			</div>
		</div>
		<!-- end of news loop -->
		@endforeach
		<div class="clearfix"></div>
		</div>
	</div>
</section>
<!-- //our blog -->
 <!-- footer -->
 <footer>
		<div class="agileits-w3layouts-footer">
			<div class="container">
				<div class="col-md-4 w3-agile-grid" id="aboutUs">
					<h5>About Us</h5>
					<p>Trusted and reliable motor rental</p>
					<div class="footer-agileinfo-social">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
							<li><a href="#"><i class="fa fa-vk"></i></a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-md-4 w3-agile-grid">
					<h5>Address</h5>
					<div class="w3-address">
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Phone Number</h6>
								<p>+6288888888</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Email Address</h6>
								<p>Email :<a href="mailto:example@email.com"> cs@rapahdirtbikerental.com</a></p>
							</div>
							<div class="clearfix"> </div>
						</div>
				
					</div>
				</div>
				<div class="col-md-4 w3-agile-grid">
					<h5>Locations</h5>
					<div class="w3ls-post-grids">
						<div class="w3ls-post-grid">
						<div class="w3-address-left">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Location</h6>
								<p> Jl Raya Menanga 
								Telephone : +62 8588888888
								</p>
							</div>
							<div class="clearfix"> </div>	
						
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="copyright">
			<div class="container">
				<p>© 2020 RapahDirtBikeRental. All rights reserved </p>
			</div>
		</div>
	</footer>
	<!-- //footer -->

  <!-- Modal -->
  <div class="modal fade" id="bookModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
     
		<div class="banner-form-agileinfo">
									<h5>Book your bike Now</h5>
									<p></p>
									<form method="post" action="{{url('/motorbook')}}">
									@csrf
										<input type="text" class="email" name="name" placeholder="Name" required>
                                        <input type="tel" class="tel" name="phone" placeholder="Phone Number" required>
										<input type="text" class="email" name="email" placeholder="Email" required>
										<input type="text" id="startDate" name="startDate" placeholder="From" required>
										<input type="text" id="endDate" name="endDate" placeholder="To" required>
										<select class="form-control m-md-3" name="brand" id="motorBrand" required>
                               			</select>
										   <select class="form-control m-md-3" name="type" id="motorType" required>
                               			</select>
										<input type="submit" class="hvr-shutter-in-vertical" value="Book">  	
									</form>
								</div>
        </div>
    
      </div>

    </div>
  </div>
@endsection

