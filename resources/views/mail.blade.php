<h1>Hi, {{ $name }}</h1>
<p>Thanks you for booking our bike</p>
<p>Please wait our staff to contact and confirm your booking</p>
<p>Your booking may change depend on bike avability</p>
<div>
    <h3>Booking Detail</h3>
</div>
<div>
    <div class="row">
        <div class="col-md-6">
            Name
        </div>
        <div class="col-md-6">
            {{$name}}            
         </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Phone
        </div>
        <div class="col-md-6">
            {{$phone}}            
         </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Email
        </div>
        <div class="col-md-6">
            {{$email}}            
         </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Booking Duration
        </div>
        <div class="col-md-6">
            {{$startDate}} To {{$endDate}}            
         </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Motor
        </div>
        <div class="col-md-6">
            {{$motorType}} - {{$motorBrand}}         
         </div>
    </div>

</div>