@extends('layouts.adminbookinglayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Booking Admin</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <form action="{{url('/')}}" method="post" enctype="multipart/form-data">
                    @csrf

                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Reg Number:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="regNumber" class="form-control m-md-3" placeholder="XX XXXX XXX" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Type:</label>
                            </div>
                            <div class="row">
                                <select class="form-control m-md-3" name="type" id="motorType" >
                               </select>
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Brand:</label>
                            </div>
                            <div class="row">
                               <select class="form-control m-md-3" name="brand" id="motorBrand" >
                               </select>
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Year:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="year" id="motoryear" class="form-control m-md-3" placeholder="New Type" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Rate (IDR):</label>
                            </div>
                            <div class="row">
                                <input type="text" name="rate" class="form-control m-md-3" placeholder="Rate" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Photo:</label>
                            </div>
                            <div class="row">
                                <input type="file" name="pic" class="form-control m-md-3" />
                            </div>
                        </div>
                        <div class="form-group m-md-5">
                            <div class="row">
                                <label>Status:</label>
                            </div>
                            <div class="row">
                               <select class="form-control m-md-3" name="status">
                               <option selected>Select status</option>
                               <option value="Available">Avaliable</option>
                               <option value="Maintenance">Maintenance</option>
                               <option value="Occupied">Occupied</option>
                               <option value="Broken">Broken</option>
                               </select>
                            </div>
                        </div>
                        <div class="form-group m-md-5">

                            <div class="col-md-10"></div>
                            <div class="col-md-2 pull-right">
                                <button type="submit" class="btn btn-primary btn-outline">
                                <span class="glyphicon glyphicon-floppy-disk"></span>
                                Save</button>
                            </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
