<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
       <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admintheme/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('admintheme/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admintheme/dist/css/adminlte.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('summernote/dist/summernote.min.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand custom-title" href="{{ url('/home') }}">
                <div class="custom-title">
                   Rapah Bali Dirt Bike
                    </div>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <div class="pull-right">
                        @include('layouts/menubar')
                    </div>

                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" defer></script>
<script src="{{ url('/') }}/unisharp/laravel-ckeditor/ckeditor.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admintheme/dist/js/adminlte.min.js')}}"></script>

<script>
   $(document).ready(function() {
    window.GetDetail = function(param){
        $.getJSON("{{ url('/') }}/newsdetail/"+param, function(data){
            $('#pedit').html('<a href="{{url("newsedit")}}/'+data[0].id+'"><button class="btn btn-warning btn-outline m-md-3"><i class="fa fa-pencil-square-o"></i>Edit</button></a>');
            $('#pdel').html('<a href="{{url("newsdelete")}}/'+data[0].id+' "><button class="btn btn-danger btn-outline m-md-3 del"><i class="fa fa-trash-o"></i>Delete</button></a>');
            $('.table-responsive #pid').html(data[0].id);
            $('.table-responsive #ptitle').html(data[0].title);
            $('.table-responsive #pcreated').html(data[0].created_date);
            $('.table-responsive #pupdate').html(data[0].update_date);
            $('.table-responsive #pcontent').html(data[0].content);
            $('.table-responsive #ppic').html('<img src="{{url("/newscap")}}/'+data[0].photocap+'" style="max-width: 50%" >');
            $('.table-responsive #pstatus').html(data[0].status);
          });
          $('#myModal').modal('show');
    };

    $('.del').click(function() {
    return confirm('Are You Sure ?')
  });

  

    window.InputTypePop = function(param){
          $('#typeModal').modal('show');
    };
    window.InputBrandPop = function(param){
          $('#brandModal').modal('show');
    };

    $('.del').click(function() {
    return confirm('Are You Sure ?')
    });

    $('#example').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [[1,'asc']],
                "paging": true,
                "lengthMenu": [10, 25, 50, 75, 100],
                ajax: {
                    url: "{{url('/newsshowall')}}",
                },
                "bLengthChange": false,
                "pageLength": 10,
                "columns":[
                    {"data":"id"},
                    {"data":"title"},
                    {"data":"created_date"},
                    {"data":"update_date"},
                    {"data":"status"},
                    {"render":function(data,type,row){
                        return'<button onclick="GetDetail('+row.id+')" class="btn btn-info btn-outline btn-sm"><i class="fa fa-eye"></i> &nbsp;Detail</button>';
                    }}
                ],
                "initComplete": function(){
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                    });
                },
                "bDestroy": true,
                "language":{
                    "emptyTable":"No data available"
                }
            });

    $("#datepicker").datepicker( {
        format: "yyyy",
        orientation: "bottom",
        viewMode: "years",
        minViewMode: "years",
        changeYear:true
    });

    $("#motoryear").datepicker({
        format: "yyyy",
        weekStart: 1,
        orientation: "bottom",
        keyboardNavigation: false,
        viewMode: "years",
        minViewMode: "years"
    });

 
     
} );

</script>
</html>
