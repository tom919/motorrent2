<ul class="navbar-nav ml-auto">
    <!-- Authentication Links -->
    @guest

    @else
    <li class="nav-item px-2 pt-sm-2"><a href="{{url('/motor')}}">Motor</a></li>
    <li class="nav-item px-2 pt-sm-2">Booking</li>
    <li class="nav-item px-2 pt-sm-2">Customer</li>
    <li class="nav-item px-2 pt-sm-2">Product</li>
    <li class="nav-item px-2 pt-sm-2"><a href="{{url('/news')}}">News</a></li>
    <li class="nav-item px-2 pt-sm-2 btn-group"> 
        <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Setting
        </a>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a class="dropdown-item" href="{{url('/motortypesetting')}}">Motor Type</a>
        <a class="dropdown-item" href="{{url('/motorbrandsetting')}}">Motor Brand</a>
        <a class="dropdown-item" href="#">Account</a>
        <a class="dropdown-item" href="{{url('/editpass')}}">Password</a>
    </div>
    
    </li>
    <li class="nav-item px-2 pt-sm-2">
        <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
         {{ __('Logout') }}
     </a>

     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
         @csrf
     </form>
    </li>
    @endguest
</ul>
