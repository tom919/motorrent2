<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
       <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admintheme/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('css/ionicons2.0.1.css')}}">
      <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('admintheme/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admintheme/dist/css/adminlte.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('dt/datatables.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap3.4.1.css')}}">
    <link rel="stylesheet" href="{{ asset('fa/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/datepicker1.6.4.css')}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    <div class="custom-title">
                    Rapah Bali Dirt Bike
                    </div>
                    
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <div class="pull-right">
                        @include('layouts/menubar')
                    </div>

                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>

<script src="{{ asset('js/jquery341.js')}}"></script>
<script src="{{ asset('js/bsdatepicker164.js')}}"></script>
<script src="{{ asset('js/bootstrap341.js')}}"></script>
<script src="{{ asset('dt/datatables.js')}}" defer></script>
<!-- AdminLTE App -->
<script src="{{ asset('admintheme/dist/js/adminlte.min.js')}}"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [[1,'asc']],
                "paging": true,
                "lengthMenu": [10, 25, 50, 75, 100],
                ajax: {
                    url: "{{url('/motorshowall')}}",
                },
                "bLengthChange": false,
                "pageLength": 10,
                "columns":[
                    {"data":"id"},
                    {"data":"reg_number"},
                    {"data":"motor_type"},
                    {"data":"motor_brand"},
                    {"data":"rate"},
                    {"render":function(data,type,row){
                        return'<button onclick="GetDetail('+row.id+')" class="btn btn-info btn-outline btn-sm"><i class="fa fa-eye"></i> &nbsp;Detail</button>';
                    }}
                ],
                "initComplete": function(){
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                    });
                },
                "bDestroy": true,
                "language":{
                    "emptyTable":"No data available"
                }
            });
} );
</script>
</html>
