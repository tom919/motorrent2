<!DOCTYPE html>
<html lang="en">
<head>
<title>Rapah Bali Dirt Bike Rental</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="{{url('maintemp/css/flexslider.css')}}" type="text/css" media="all" /><!-- for testimonials -->

<!-- css files -->
<link rel="stylesheet" href="{{ asset('css/bootstrap3.4.1.css')}}">
<link rel="stylesheet" href="{{url('maintemp/css/style.css')}}" type="text/css" media="all" /> <!-- Style-CSS --> 
<link rel="stylesheet" href="{{url('maintemp/css/font-awesome.css')}}"> <!-- Font-Awesome-Icons-CSS -->
<link rel="stylesheet" href="{{ asset('css/datepicker1.6.4.css')}}">
<link rel="stylesheet" href="{{ asset('maintemp/css/lightbox.css')}}"> <!-- portfolio-CSS -->
<!-- //css files -->

<!-- web-fonts -->
<link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300&display=swap" rel="stylesheet"> 
<!-- //web-fonts -->
</head>
<body>
<div class="header">
		<nav class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a href="{{url('/')}}">Rapah Bali Dirt Bike</a></h1>
					</div>
			
					<!-- navbar-header -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
                            <li><a class="hvr-underline-from-center active" href="{{url('/')}}">Home</a></li>
							<li><a href="#aboutUs" class="hvr-underline-from-center">About Us</a></li>
							<li><a href="{{url('/motorgallery')}}" class="hvr-underline-from-center">Motor</a></li>
                            <!-- <li><a href="#" data-toggle="dropdown"><span data-hover="ShortCodes">Motor</span><span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href=""><span data-hover="Icons"></span>Offroad Bikes</a></li>
                                    <li><a href=""><span data-hover="Typograpghy">Scooter</span></a></li>
                                    <li><a href=""><span data-hover="Typograpghy">Big Scooter</span></a></li>
                                    <li><a href=""><span data-hover="Typograpghy">Sports</span></a></li>
								</ul>
							</li>	 -->
							<li><a href="#blog" class="hvr-underline-from-center">News</a></li>
							<li><a href="#blog" class="hvr-underline-from-center">Shop</a></li>	
							<li><a href="#aboutUs" class="hvr-underline-from-center">Contact</a>
						</ul>
					</div>

					<div class="clearfix"> </div>	
				</nav>
	
    </div>
    




    @yield('content')



   



<!-- js-scripts -->					

<!-- js -->
<script type="text/javascript" src="{{asset('maintemp/js/jquery-2.1.4.min.js')}}"></script>
	<script src="{{ asset('js/bootstrap341.js')}}"></script>
	<script src="{{ asset('js/bsdatepicker164.js')}}"></script>
	<script src="{{url('maintemp/js/responsiveslides.min.js')}}"></script>
	<script src="{{url('maintemp/js/jquery.flexslider.js')}}" defer></script>
	<script src="{{url('maintemp/js/simplePlayer.js')}}"></script>
	<script src="{{url('maintemp/js/counterup.min.js')}}"></script> 
	<script src="{{url('maintemp/js/waypoints.min.js')}}"></script> 
<!-- //js -->	

<!-- start-smoth-scrolling -->
<script src="{{asset('maintemp/js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{asset('maintemp/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('maintemp/js/easing.js')}}"></script>


<script type="text/javascript">
	jQuery(document).ready(function($) {

		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/

		window.GetNewsDetail = function(param){
        $.getJSON("{{ url('/') }}/newsdetailfront/"+param, function(data){  
			console.log("title data "+data[0].title); 
            $('#ptitle').html(data[0].title);
            $('#pcreated').html(data[0].created_date);
            $('#pcontent').html(data[0].content);
            $('#pcap').html('<img src="{{url("/newscap")}}/'+data[0].photocap+' class="img-responsive" >');
          });
          $('#myModal').modal('show');
    	};
								
			$().UItoTop({ easingType: 'easeOutQuart' });
			$('#motorBrand').append('<option value="" selected>Choose Motor Brand </option>');
			$("#motorBrand").focus(function(){
            $('#motorBrand').empty()
			
			
            $.ajax({
                    type: "GET",
                    url: "{{url('/motorbrandf')}}",
                    success: function(data){
                        // Parse the returned json data
                        var opts = data;
						
                        // Use jQuery's each to iterate over the opts value
                        $.each(opts, function(i, d) {
                            // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                            $('#motorBrand').append('<option value="' + d.brand_name + '">' + d.brand_name + '</option>');
                        });
                    }
                });
        });
		$('#motorType').append('<option value="" selected>Choose Motor Type </option>');
        $("#motorType").focus(function(){
            $('#motorType').empty()
    
            $.ajax({
                    type: "GET",
                    url: "{{url('/motortypef')}}",
                    success: function(data){
                        // Parse the returned json data
                        var opts = data;
                        
                        // Use jQuery's each to iterate over the opts value
                        $.each(opts, function(i, d) {
                            // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                            $('#motorType').append('<option value="' + d.name + '">' + d.name + '</option>');
                        });
                    }
                });
        });


		window.InputBook = function(param){
			$('#bookModal').modal('show');
		};

		$( "#startDate" ).datepicker();
		$( "#endDate" ).datepicker();
		
	});
	</script>
	<!-- //here ends scrolling icon -->
<!-- start-smoth-scrolling -->

<!-- Baneer-js -->

	<script>
		$(function () {
			$("#slider").responsiveSlides({
				auto: true,
				pager:false,
				nav: true,
				speed: 1000,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>
<!-- //Baneer-js -->

<!-- banner bottom video script -->

			<script>
				$("document").ready(function() {
					$("#video").simplePlayer();
				});
</script>
<!-- //banner bottom video script -->

<!-- Stats-Number-Scroller-Animation-JavaScript -->
		
				<script>
					jQuery(document).ready(function( $ ) {
						$('.counter').counterUp({
							delay: 100,
							time: 1000
						});
					});
				</script>
<!-- //Stats-Number-Scroller-Animation-JavaScript -->


<!-- FlexSlider-JavaScript -->

	<script type="text/javascript">
				$(window).load(function(){
				$('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
						$('body').removeClass('loading');
					}
			});
		});
	</script>
<!-- //FlexSlider-JavaScript -->
		

<!-- //js-scripts -->
</body>
</html>